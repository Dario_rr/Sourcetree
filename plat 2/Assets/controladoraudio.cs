using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controladoraudio : MonoBehaviour
{
   
    public static controladoraudio Instance;
    private AudioSource AudioSource;


    private void Awake()
    {
        if (Instance == null) 
        {
          Instance = this;
          DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);

        }
        AudioSource = GetComponent<AudioSource>();
    }

    
   public void EjecutarSonido(AudioClip sonido)
    {
        AudioSource.PlayOneShot(sonido);
    }
}
