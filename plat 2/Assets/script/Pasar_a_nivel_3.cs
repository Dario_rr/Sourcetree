using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pasar_a_nivel_3 : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D other)
    {

        SceneManager.LoadScene("nivel_3");
        Time.timeScale = 1.0f;
    }
    private void Awake()
    {

        gameObject.SetActive(true);
    }
}
