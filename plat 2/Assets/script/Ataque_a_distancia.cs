using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ataque_a_distancia : MonoBehaviour
{
    public Transform puntoDeDisparo; // El punto desde donde se disparar� el proyectil
    public GameObject prefabProyectil; // El prefab del proyectil que se disparar�

    // M�todo para disparar el proyectil
    public void Disparar()
    {
        // Instancia el proyectil en el punto de disparo
        Instantiate(prefabProyectil, puntoDeDisparo.position, puntoDeDisparo.rotation);
    }

    // Llama a este m�todo cuando quieras que el personaje dispare
    private void Update()
    {
        if (Input.GetButtonDown("Ataque")) 
        {
            Disparar();
        }
    }
}
